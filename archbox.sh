#!/bin/bash

shopt -s extglob

bashroot=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
cd "$bashroot"
username=huazhechen

baselist='linux coreutils bash diffutils tar zip unzip unrar lzop dhcpcd iputils inetutils net-tools netctl wpa_supplicant which procps-ng man-db man-pages ntfsprogs btrfs-progs patch dosfstools terminus-font whois licenses vi bash-completion pkgfile dialog sed rsync ntp gcc-libs-multilib systemd-sysvcompat arch-install-scripts'
extralist='grub dnsutils dnsmasq ebtables alsa-utils openssh sudo make pkgtools xinetd cryptsetup jfsutils logrotate lvm2 mdadm pcmciautils reiserfsprogs usbutils xfsprogs'
xorglist='xorg-server xorg-drivers'
gnomecorelist='gdm gnome-shell'
gnomebaselist='gnome-control-center gnome-terminal gnome-keyring gnome-settings-daemon gnome-tweak-tool nautilus networkmanager seahorse'
gnomeutillist='evolution baobab gnome-disk-utility gnome-system-monitor gnome-logs gnome-font-viewer gnome-color-manager gnome-calculator dconf-editor gnome-power-manager'
gnomeextralist='numix-themes numix-circle-icon-theme-git gnome-icon-theme gnome-icon-theme-extras gnome-icon-theme-symbolic yelp gnome-user-docs gnome-user-share gnome-backgrounds xdg-user-dirs-gtk gnome-sound-recorder vino vinagre'
medialist='totem rhythmbox cheese eog eog-plugins gedit gedit-code-assistance evince sushi'
officelist='vim libreoffice-fresh libreoffice-fresh-zh-CN'
gstreamerlist='gst-libav gst-vaapi gst-plugins-base gst-plugins-base-libs gst-plugins-good gst-plugins-bad gst-plugins-ugly'
weblist='firefox firefox-i18n-zh-cn flashplugin'
utillist='file-roller filezilla inkscape gimp'
fontlist='ttf-dejavu ttf-arphic-ukai ttf-arphic-uming wqy-microhei adobe-source-han-sans-cn-fonts'
inputlist='ibus-rime'
developlist='intellij-idea-community-edition git maven android-sdk android-sdk-build-tools android-sdk-platform-tools android-udev multilib-devel jdk8-openjdk'

packagelist="$baselist $extralist $xorglist $gnomecorelist $gnomebaselist $gnomeutillist $gnomeextralist $medialist $officelist $gstreamerlist $weblist $utillist $fontlist $inputlist $developlist"

installsystem(){
target=$1

echo "ADJUST TIME"
ntpdate asia.pool.ntp.org && hwclock -w&

echo "USE PKG CACHE IN DATA"
forceln "/data/system/linux/pkg/" "/var/cache/pacman/"

echo "INSTALL PACKAGE"
pacstrap -cd "$target" $packagelist || cleanup

echo "CREATE DATA MOUNT POINT"
mkdir -p "$target/data"
mount --bind "/data/" "$target/data/"

echo "CREATE PKG AND SDK LINK"
forceln "/data/develop/android-sdk/platforms/" "$target/opt/android-sdk/"
forceln "/data/system/linux/pkg/" "$target/var/cache/pacman/"

echo "PACKAGE CACHE CLEAN"
arch-chroot "$target" paccache -r
arch-chroot "$target" paccache -ruk0

echo "CHROOT FOR USER AND PASSWORD"
arch-chroot "$target" echo "MODIFY ROOT PASSWORD"
arch-chroot "$target" passwd
arch-chroot "$target" useradd -m "$username"
arch-chroot "$target" echo "MODIFY $username PASSWORD"
arch-chroot "$target" passwd "$username"

echo "MODIFY SETTINGS FILE"
rm -rf "$target/home/$username" 
rm -rf "$target/etc/grub.d" 
cp -rf "$bashroot/settings/"* "$target" 
sed -i "/AutomaticLogin/d" "$target/etc/gdm/custom.conf" 
sed -i "/^\[daemon\]$/aAutomaticLogin=$username" "$target/etc/gdm/custom.conf" 
sed -i "/^\[daemon\]$/aAutomaticLoginEnable=true" "$target/etc/gdm/custom.conf" 
sed -i "/^MODULES/s/\"$/radeon i915 nouveau\"/g" "$target/etc/mkinitcpio.conf" 
sed -i "/^HOOK/s/\"$/ consolefont\"/g" "$target/etc/mkinitcpio.conf" 
sed -i "/^HOOK/s/\"fsck/\"/g" "$target/etc/mkinitcpio.conf" 
sed -i "/^HOOK/s/ fsck//g" "$target/etc/mkinitcpio.conf" 
echo 'Arch-Hua' > "$target/etc/hostname"
echo 'LANG="en_US.UTF-8"' > "$target/etc/locale.conf"
echo 'export LANG=en_US.UTF-8' >> "$target/etc/profile"
echo 'export LANG=zh_CN.UTF-8' >> "$target/home/$username/.profile"
sed -i "/^#en_US.UTF-8/s/#//g" "$target/etc/locale.gen"
sed -i "/^#zh_CN.UTF-8/s/#//g" "$target/etc/locale.gen"
echo 'FONT=ter-132b' > "$target/etc/vconsole.conf"
cat >> "$target/etc/ssh/ssh_config" << EOF
Host *
    StrictHostKeyChecking no
    UserKnownHostsFile /dev/null
Host bitbucket.*
    HostName bitbucket.org
    PubkeyAuthentication yes
    IdentityFile /data/develop/ssh/id_rsa
EOF

echo "MODIFY PRIVALIGE"
arch-chroot "$target" bash << EOF
chmod 0440 /etc/sudoers
chown -Rf $username:$username /home/$username
chmod -Rf 755 /home/$username
chown -Rf root:root /root/
chmod -Rf 755 /root/
chmod 755 /etc/grub.d/*
chmod 600 /data/develop/ssh/*
EOF

echo "SETTINGS BY COMMAND"
arch-chroot "$target" bash << EOF
grub-install /dev/sda 
grub-mkconfig > /boot/grub/grub.cfg
systemctl enable graphical.target 
systemctl enable sshd.service 
systemctl enable gdm.service 
systemctl enable NetworkManager.service 
systemctl enable ntpd.service 
ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime 
locale-gen
EOF

echo "GENERATE FSTAB"
genfstab "$target"
genfstab "$target" >> "$target/etc/fstab"

echo "MKINITCPIO"
arch-chroot "$target" mkinitcpio -p linux

echo "HIDE ICONS"
hideicons
echo "CREATE LIVE"
generatelive "$target"
}

generatelive(){
target=$1
WORKDIR=`mktemp -d`

echo "CREATE DATA DIR"
mkdir "$WORKDIR/data"

echo "USE PKG CACHE IN DATA"
forceln "/data/system/linux/pkg/" "/var/cache/pacman/"

echo "INSTALL PACKAGE"
pacstrap -cd "$WORKDIR" $baselist || cleanup

echo "MODIFY SETTINGS FILE"
cp -rf "$bashroot/settings/"* "$WORKDIR"
echo 'Arch-Hua' >"$WORKDIR/etc/hostname"
echo 'LANG="en_US.UTF-8"' > "$WORKDIR/etc/locale.conf"
echo 'export LANG=en_US.UTF-8' >> "$WORKDIR/etc/profile"
sed -i "/^#en_US.UTF-8/s/#//g" "$WORKDIR/etc/locale.gen"
sed -i "/^#zh_CN.UTF-8/s/#//g" "$WORKDIR/etc/locale.gen"
echo 'FONT=ter-132b' > "$WORKDIR/etc/vconsole.conf"
cat >> "$WORKDIR/etc/ssh/ssh_config" << EOF
Host *
    StrictHostKeyChecking no
    UserKnownHostsFile /dev/null
Host bitbucket.*
    HostName bitbucket.org
    PubkeyAuthentication yes
    IdentityFile /data/develop/ssh/id_rsa
EOF

echo "SETTINGS BY COMMAND"
arch-chroot "$WORKDIR" bash <<EOF
ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
EOF

echo "MODIFY LIVE SETTINGS FILE"
sed -i "/^MODULES/s/\"$/radeon i915 nouveau\"/g" "$WORKDIR/etc/mkinitcpio.conf" 
sed -i "/^HOOK/s/\"$/ consolefont\"/g" "$WORKDIR/etc/mkinitcpio.conf" 
sed -i "/^HOOK/s/\"autodetect/\"/g" "$WORKDIR/etc/mkinitcpio.conf" 
sed -i "/^HOOK/s/ autodetect//g" "$WORKDIR/etc/mkinitcpio.conf" 
sed -i "/^HOOK/s/\"fsck/\"/g" "$WORKDIR/etc/mkinitcpio.conf" 
sed -i "/^HOOK/s/ fsck//g" "$WORKDIR/etc/mkinitcpio.conf" 
sed -i "/^HOOK/s/\"udev/\"/g" "$WORKDIR/etc/mkinitcpio.conf" 
sed -i "/^HOOK/s/ udev//g" "$WORKDIR/etc/mkinitcpio.conf" 
sed -i "/^HOOK/s/\"block/\"/g" "$WORKDIR/etc/mkinitcpio.conf" 
sed -i "/^HOOK/s/ block//g" "$WORKDIR/etc/mkinitcpio.conf" 
sed -i "/^install_modules/d" "$WORKDIR/usr/bin/mkinitcpio" 
sed -i "/^ldconfig/d" "$WORKDIR/usr/bin/mkinitcpio" 
sed -i "/^rootdev/,\$d" "$WORKDIR/usr/lib/initcpio/init"
echo 'exec $init' >> "$WORKDIR/usr/lib/initcpio/init"

echo "ADD HOOK AND MODULES"
arch-chroot "$WORKDIR/" mkinitcpio -s -d /mnt/
cp -na "$WORKDIR/mnt/"* "$WORKDIR"

echo "REMOVE UNUSED FILES"
mkdir -p "$bashroot/live"
mv -f "$WORKDIR/boot/vmlinuz-linux" "$bashroot/live/vmlinuz-linux"
rm -rf "$WORKDIR/boot/"
rm -rf "$WORKDIR/var/lib/pacman/sync/*"

echo "GENERATE LIVE FILES"
cd "$WORKDIR"
find . | bsdcpio -oH newc| gzip > "$bashroot/live/initramfs-live.gz"
cp -rf "$bashroot/live" "$target/boot/"
}


hideicon(){
filename=$(basename $application)
if [[ $application == *libreoffice* ]]
then
filename=libreoffice-$filename
fi
cp -f "$application" "$target/home/$username/.local/share/applications/$filename"
sed  -i "/NoDisplay/d" "$target/home/$username/.local/share/applications/$filename"
sed  -i "/\[Desktop\ Entry\]/a\NoDisplay=true" "$target/home/$username/.local/share/applications/$filename"
}

hideicons(){
application="$target/usr/share/applications/android-sdk.desktop"
hideicon
application="$target/usr/share/applications/avahi-discover.desktop"
hideicon
application="$target/usr/share/applications/eog.desktop"
hideicon
application="$target/usr/share/applications/bssh.desktop"
hideicon
application="$target/usr/share/applications/bvnc.desktop"
hideicon
application="$target/usr/share/applications/evince.desktop"
hideicon
application="$target/usr/share/applications/flash-player-properties.desktop"
hideicon
application="$target/usr/share/applications/org.gnome.FileRoller.desktop"
hideicon
application="$target/usr/share/applications/org.gnome.font-viewer.desktop"
hideicon
application="$target/usr/share/applications/ibus-setup.desktop"
hideicon
application="$target/usr/lib/libreoffice/share/xdg/base.desktop"
hideicon
application="$target/usr/lib/libreoffice/share/xdg/calc.desktop"
hideicon
application="$target/usr/lib/libreoffice/share/xdg/draw.desktop"
hideicon
application="$target/usr/lib/libreoffice/share/xdg/impress.desktop"
hideicon
application="$target/usr/lib/libreoffice/share/xdg/math.desktop"
hideicon
application="$target/usr/lib/libreoffice/share/xdg/writer.desktop"
hideicon
application="$target/usr/share/applications/nm-connection-editor.desktop"
hideicon
application="$target/usr/share/applications/qv4l2.desktop"
hideicon
application="$target/usr/share/applications/seahorse.desktop"
hideicon
application="$target/usr/share/applications/yelp.desktop"
hideicon
}

pushsettings(){
cd /data/system/linux/
git add -A ./
git commit -m "From Archbox"
git push
}

downloadhosts(){
cd "/etc/"
curl -O https://raw.githubusercontent.com/txthinking/google-hosts/master/hosts
if [ $? -ne 0 ]; then
    sethosts
fi
sed -i s/"\r"//g "/etc/hosts"
}

syncsettings(){
SYNC_OPT="-avhHP --force --existing"
SYNC_HOMES=`find /data/system/linux/settings/ -maxdepth 1 -type d | sed  "/^\/data\/system\/linux\/settings\/$/d"`
for SYNC_HOME in $SYNC_HOMES
do
    SYNC_SERVER=$(basename $SYNC_HOME)
    rsync $SYNC_OPT /$SYNC_SERVER/ $SYNC_HOME
done
}


backupfiles(){
SYNC_SERVER=$1
SYNC_HOME=$2
SYNC_OPT="-avhHP --del --force"
rsync $SYNC_OPT $SYNC_SERVER/ $SYNC_HOME
}

forceln(){
src=$1
dst=$2
if [ ! -L "$dst/"$(basename $src) ]; then
    rm -rf "$dst/"$(basename $src)
fi
ln -sfn "$src" "$dst"
}

usage() {
    cat <<EOF
usage: ${0} <command> [...]

  Commands:
   install     Install a new system.
   live        Generate a live system.
   hide        Hide icons.
   hosts       Download hosts.
   sync        Sync linux settings to /data/.
   push        Push linux settings to bitbucket.
   backup      Backup from src to dst.
EOF
}

cleanup(){
    while (($#>0))
    do
        echo "CLEAN $1"
        rm -rf "$1"
        shift
    done
    if [[ "$WORKDIR" ]]; then
        echo "CLEAN $WORKDIR"
        rm -rf "$WORKDIR"
    fi
    echo "--------------------------------"
    unlink "$FIFO/archbox.fifo" &>/dev/null
    rm -rf "$FIFO" &>/dev/null
    exit
}

parseopts(){
    local target
    case $1 in
        install)
            shift
            target=$1
            if [ -z $target ]; then
                echo "usage: ${0} install target"
                cleanup
            fi
            installsystem $target        
            ;;
        live)
            shift
            target=$1
            if [ -z $target ]; then
                echo "usage: ${0} live target"
                cleanup
            fi
            generatelive $target
            ;;
        hide)
            hideicons
            ;;
        hosts)
            downloadhosts
            ;;
        sync)
            syncsettings
            ;;
        push)
            pushsettings
            ;;
        backup)
            shift
            src=$1
            if [ -z $src ]; then
                echo "usage: ${0} backup src dst"
                cleanup
            fi
            shift
            dst=$1
            if [ -z $dst ]; then
                echo "usage: ${0} backup src dst"
                cleanup
            fi
            backupfiles $src $dst
            ;;
        *)
	    usage
            ;;
    esac
}

if [ "$(whoami)" != "root" ]; then
    exec sudo bash ${0} $*
    exit
fi

FIFO=`mktemp -d`
mkfifo "$FIFO/archbox.fifo"
cat "$FIFO/archbox.fifo" | tee -a /var/log/archbox.log &

exec &>>"$FIFO/archbox.fifo"

trap 'cleanup' INT
trap 'cleanup' TERM

echo "++++++++++++++++++++++++++++++++"

parseopts $*

cleanup

